<?php
//Запускаем сессию
session_start();

require_once 'CDB.php';
require_once 'CToken.php';
require_once 'CUsers.php';

$config = new Config();
$db = new CDB();
$token = new CToken();
$users = new CUsers();

/*
    Проверяем была ли отправлена форма, то есть была ли нажата кнопка Войти. Если да, то идём дальше, если нет, то выведем пользователю сообщение об ошибке, о том, что он зашёл на эту страницу напрямую.
*/
if(isset($_POST["btn_submit_reg"]) && !empty($_POST["btn_submit_reg"])){
    //Добавляем файл подключения к БД

    $mylogin = trim($_POST["login"]);
    $mypassword = trim($_POST["password"]);

//    // Место для обработки пароля
//    if(isset($_POST["password"])){
//        if(!empty($password)){
//            $password = htmlspecialchars($password, ENT_QUOTES);
//
//            //Шифруем пароль
//            //TODO поменять на модно-современную функцию шифрования
//            $password = md5($password."top_secret");
//        }
//        else
//            {
//            // Сохраняем в сессию сообщение об ошибке.
//            $_SESSION["error_messages"] .= "<p class='mesage_error' >Укажите Ваш пароль</p>";
//
//            //Возвращаем пользователя на страницу регистрации
//            header("HTTP/1.1 301 Moved Permanently");
//            header("Location: ".$config->address_site."form_auth.php");
//
//            //Останавливаем скрипт
//            exit();
//            }


    //Запрос в БД на проверку повтора пользователя
    if (!$users->getidfromlogin($mylogin)) {
        $users->createuser($mylogin, $mypassword);
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".$config->address_site."profile.php");
    }
     else
         {
            //Возвращаем пользователя на страницу регистрации
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ".$config->address_site."form_reg.php?c");
            //Останавливаем скрипт
            exit();
    }


        //======= Обработка галочки " запомнить меня " =======
        //Проверяем, если галочка была поставлена
        if(isset($_POST["remember_me"])){
            //Добавляем созданный токен в базу данных
            $token->createtokens($mylogin);

        }
        else //не нажал галку - созранить пароль

            if(isset($_COOKIE['tokenref'])){
                $mytoken = $token->deletetokens($_COOKIE['tokenref']);
            }

        //место для добавления данных в сессию
        // Если введенные данные совпадают с данными из базы, то сохраняем логин и пароль в массив сессий.
        $_SESSION['user'] = $mylogin;
//    $_SESSION['user_hash'] = $config->getmyhash(mylogin);
//    $_SESSION['password'] = $password;

        //Возвращаем пользователя в профиль
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".$config->address_site."profile.php");



}
else
{
    exit("<p><strong>Ошибка!</strong> Вы зашли на эту страницу напрямую, поэтому нет данных для обработки. Вы можете перейти на <a href=".$config->address_site."> главную страницу </a>.</p>");
}
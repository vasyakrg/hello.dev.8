<?php
/**
 * Created by PhpStorm.
 * User: vasyakrg
 * Date: 2018-12-04
 * Time: 13:23
 */
require_once 'CConfig.php'; // тут я загружаю параметры при подключении к базе

class CDB //класс для управления базой
{
    public function __construct()
    {
        $config = new Config();
        $this->connect = new PDO("pgsql:host=$config->dbhost;dbname=$config->dbname", "$config->dbuser", "$config->dbpass"); //я подлючаюсь в PostgreSQL!
        $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //указываю атрибуты, в данном случае что бы видеть ошибки при работе с базой
        if (!($this->connect)) {
            echo 'error connect to DB! brexit';
            die;
        }
    }
}



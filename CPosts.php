<?php
require_once 'CDB.php';
require_once 'CUsers.php';
require_once 'CConfig.php';

/**
 * Class CPosts
 * Класс для работы с постами пользователей
 */
class CPosts
{
    /**
     * Выводит в массив посты конкретного $user с $limit на кол-во постов
     * @param $user     логин пользователя из таблицы users
     * @param $limit    лимит на кол-во элементов в массиве
     * @return array    ассоциативный массив с постами
     */
    function viewposts($user, $limit)
    { //выводит список постов от конкретного юзера с указанных лимитом строк
        $db = new CDB();
        $users = new CUsers();
        $query = 'select posts.idpost, posts.post
               from posts
               where posts.iduser = :iduser ORDER BY postdate DESC LIMIT :limit'; //сортирую по базе
        $myresult = $db->connect->prepare($query);
        $myresult->execute(array(':iduser' => $users->getidfromlogin($user), ':limit' => $limit));

        $allposts = $myresult->fetchAll(PDO::FETCH_ASSOC); //выбирает все записи по конкретному id юзера

        return $allposts;
    }

    /**
     * Возвращает текст конкретного поста
     * @param $idpost
     * @return mixed
     */
    function viewpost($idpost){
        $db = new CDB();
        $query = 'select post from posts where idpost = :idpost';
        $myresult = $db->connect->prepare($query);
        $myresult->execute(array(':idpost' => $idpost));
        $post = $myresult->fetchColumn(0);
        if ($myresult) return $post; else false;
    }

    /**
     * Выводит все самые свежие посты с сортировкой по дате
     * @param $limit    лимит на кол-во элементов в массиве
     * @return array    ассоциативный массив с постами
     */
    function allposts($limit)
    { //выводит список всех поледних постов с лимитом
        $db = new CDB();
        $query = "select users.login, posts.post, date_trunc('second', posts.postdate::timestamp) from posts inner join users on posts.iduser = users.iduser ORDER BY postdate DESC LIMIT :limit";
        $myresult = $db->connect->prepare($query);
        $myresult->execute(array(':limit' => $limit));

        $allposts = $myresult->fetchAll(PDO::FETCH_ASSOC);

        return $allposts;
    }

    /**
     * Создает новый пост от имени конкретного пользователя
     * @param $login
     * @param $text
     * @return bool
     */
    function createpost($login, $text){
        $db = new CDB();
        $user = new CUsers();
        $iduser = $user->getidfromlogin($login);
        $query = 'insert into posts (iduser, post, postdate) values (:iduser, :post, current_timestamp)';
        $myresult = $db->connect->prepare($query);
        $myresult->execute(array(':iduser' => $iduser, ':post' => $text));

        if ($myresult) return true; else false;
    }

    /**
     * Удаляет конкретный пост указанного юзера
     * @param $login
     * @param $idpost
     * @return bool
     */
    function deletepost($idpost){
        $db = new CDB();
        $query = 'delete from posts where idpost = :idpost';
        $myresult = $db->connect->prepare($query);
        $myresult->execute(array(':idpost' => $idpost));

        if ($myresult) return true; else false;
    }

    /**
     * Правит конкретный пост изменяя в нем текст на новый
     * @param $idpost
     * @param $newtext
     * @return bool
     */
    function editepost($idpost, $newtext){
        $db = new CDB();
        $query = 'update posts set post = :newtext where idpost = :idpost';
        $myresult = $db->connect->prepare($query);
        $myresult->execute(array(':idpost' => $idpost, ':newtext' => $newtext));

        if ($myresult) return true; else false;
    }

}
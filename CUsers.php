<?php
require_once 'CDB.php';
require_once 'CConfig.php';

/**
 * Class CUsers
 * Класс для работы с таблицей пользователей
 */
class CUsers
{
    /**
     * Создает нового пользователя с его логином и паролем
     * @param $login
     * @param $password
     */
    function createuser($login, $password){
        $db = new CDB();
        $query = "insert into users (login, password, avatar) values (:login, :password,'img/avatar/noavatar.png')";
        $myresult = $db->connect->prepare($query);
        $myresult->execute(array(':login'=> $login, ':password' => $password));
    }
    /**
     * Возвращает id пользователя с таблицы users по его $mylogin
     * @param $mylogin
     * @return mixed    Идентификатор или false, если такого логина нет в базе
     */
    function getidfromlogin($mylogin){
        $db = new CDB();
        $query = 'select users.iduser from users where login = :login limit 1';
        $myresult = $db->connect->prepare($query);
        $myresult->execute(array(':login'=> $mylogin));

        $iduser = $myresult->fetch(PDO::FETCH_NUM);
        return $iduser[0];
    }

    /**
     * Возвращает $login пользователя с таблицы users по его $iduser
     * @param $myid
     * @return mixed
     */
    function getloginfromid($myid){
        $db = new CDB();
        $query = 'select login from users where iduser = :iduser limit 1';
        $myresult = $db->connect->prepare($query);
        $myresult->execute(array(':iduser'=> $myid));

        $login = $myresult->fetch(PDO::FETCH_NUM);
        return $login[0];

    }

    function getloginfromtoken($settoken){
        $db = new CDB();
        $myresult = $db->connect->prepare('select iduser from tokens where token = :token limit 1'); //и формирую запрос в базу
        $myresult->execute(array(':token' => $settoken)); // передаю запрос подставляя мои значениея в переменные с :

        $iduser = $myresult->fetch(PDO::FETCH_NUM);
        return $this->getloginfromid($iduser[0]);
    }

    /**
     * Возвращает true если логин\пасс пользователя найдет в базе, если не найден - false
     * @param $mylogin string
     * @param $mypass string
     * @return bool
     */
    function loginpass($mylogin, $mypass){
        $db = new CDB();
        $myresult = $db->connect->prepare('select users.login, users.password from users where login = :login AND password = :password');
        $myresult->execute(array(':login' => $mylogin, ':password' => $mypass));

        if ($myresult->fetch(PDO::FETCH_NUM) > 0)
        {
            $this->updatelastdate($mylogin, $_COOKIE['token']);
            return true;
        }
            else return false;
    }

    /**
     Обновляет дату последнего логина пользователя
     * @param $mylogin  Логин пользователя с таблицы users
     * @return bool
     */
    function updatelastdate($mylogin, $token)
    {
        $db = new CDB();
        $config = new Config();
        $users = new CUsers();
        $myresult = $db->connect->prepare('update tokens set lastdate = current_timestamp, lastip = :lastip where iduser = :userid and token = :token');
        $myresult->execute(array(':userid' => $users->getidfromlogin($mylogin), ':lastip' => $config->getmyip(), ':token' => $token));
        if ($myresult) return true;
         else return false;
    }

    /**
     Возвращяет путь на сервере, где хранится аватар конкретного юзера по его $token
     * @param $token  $token пользователя с таблицы tokens
     * @return bool|string Вернет путь до аватара на сервере или false, если токен не найден
     */
    function getavatarfromtoken($token)
    {
        $db = new CDB();
        $login = $this->getloginfromtoken($token);
        $userid = $this->getidfromlogin($login);

        $myresult = $db->connect->prepare('select users.avatar from users where iduser = :iduser limit 1');
        $myresult->execute(array(':iduser' => $userid));
        $avatar = $myresult->fetch(PDO::FETCH_COLUMN);
        if (strlen($avatar)>0)
        {
           $config = new Config();
           $path = $config->address_site.$avatar;
           return $path;
        }
         else return false;
    }

    /**
     * Возвращяет путь на сервере, где хранится аватар конкретного юзера по его $login
     * @param $login логин пользователя с таблицы users
     * @return bool|string Вернет путь до аватара на сервере или false, если логин не найден
     */
    function getavatarfromlogin($login)
    {
        $db = new CDB();
        $myresult = $db->connect->prepare('select users.avatar from users where login = :login limit 1');
        $myresult->execute(array(':login' => $login));
        $avatar = $myresult->fetch(PDO::FETCH_COLUMN);
        if (strlen($avatar)>0)
        {
            $config = new Config();
            $path = $config->address_site.$avatar;
            return $path;
        }
        else return false;
    }

    /**
     * Возвращает массив из путей аватаров пользователей
     * @return array
     */
    function getavatars()
    {
        $db = new CDB();
        $myresult = $db->connect->query('select users.avatar, users.login from users')->fetchAll(PDO::FETCH_KEY_PAIR);

        return $myresult;
    }

    /**
     * Возвращает полный путь до аватара по имени пользователя
     * @param $login имя искомого пользователя
     * @param $avatars массив из имен и путей аватаров
     * @return string полный путь
     */
    function getpathfromavatars($login, $avatars)
    {
        $config = new Config();
        $path = $config->address_site . array_search($login, $avatars);
        return $path;
    }

    /**
     * Обновляет путь к файлу аватара указанного пользователя
     * @param $login
     * @param $uploadfile
     * @return bool
     */
    function updateavatar($login, $uploadfile){
        $db = new CDB();
        $myresult = $db->connect->prepare('update users set avatar = :uploadfile where login = :login');
        $myresult->execute(array(':login' => $login, ':uploadfile' => $uploadfile));
        if ($myresult) return true; else false;
    }

    /**
     * Возвращает дату и время последнего входа по конкретному токену
     * @param $token
     * @return mixed
     */
    function getlatdatefromtoken($token){
        $db = new CDB();

        $myresult = $db->connect->prepare("select date_trunc('second', lastdate::timestamp) from tokens where token = :token limit 1");
        $myresult->execute(array(':token' => $token));
        return $lastdate = $myresult->fetch(PDO::FETCH_COLUMN);
    }

    /**
     * Возвращает последний айпи адрес по конкретному токену
     * @param $token
     * @return mixed
     */
    function getlatipfromtoken($token){
        $db = new CDB();

        $myresult = $db->connect->prepare('select lastip from tokens where token = :token limit 1');
        $myresult->execute(array(':token' => $token));
        return $lastip = $myresult->fetch(PDO::FETCH_COLUMN);
    }

    /**
     * Возвращает дату и время последнего входа по логину
     * @param $login
     * @return mixed
     */
    function getlatdatefromlogin($login){
        $db = new CDB();
        $iduser = $this->getidfromlogin($login);
        $myresult = $db->connect->prepare("select date_trunc('second', lastdate::timestamp) from tokens where iduser = :iduser order by lastdate desc limit 1");
        $myresult->execute(array(':iduser' => $iduser));
        return $lastdate = $myresult->fetch(PDO::FETCH_COLUMN);
    }

    /**
     * Возвращает последний айпи по логину
     * @param $login
     * @return mixed
     */
    function getlatipfromlogin($login){
        $db = new CDB();
        $iduser = $this->getidfromlogin($login);
        $myresult = $db->connect->prepare('select lastip from tokens where iduser = :iduser order by lastdate desc limit 1');
        $myresult->execute(array(':iduser' => $iduser));
        return $lastip = $myresult->fetch(PDO::FETCH_COLUMN);
    }
}
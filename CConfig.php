<?php
/**
 * Class Config
 * Хранит настройки сайта и системные функции
 */
Class Config
{
// public $dbhost = '10.241.98.11'; //ntsu
// public $dbhost = '192.168.9.112'; //home
// public $dbhost = '192.168.9.41'; //work
 public $dbhost = '172.18.0.1'; //hosting;
// public $dbhost = '172.20.10.2'; //sotka;

 public $dbname = 'dev8';
 public $dbuser = 'postgres';
 public $dbpass = 'secret';

    /**
     * @var string Стартовый хост сайта
     */
    public $address_site = "http://docker/hello.dev.8/";
//    public $address_site = __DIR__;

    /**
     * @var int лимит вывода постов на стартовой странице
     */
    public $limitallposts = 10;

    /**
     * @var int лимит выводп постов на странице юзера
     */
    public $limituserposts = 10;

    /**
     * @var string уникальная для каждого сайт строка, для генерации md5
     */
    public $myhash = 'cUP$m9nYb9t+Ut+@J8@%';

    /**
     * возвращает текущий внешний ip адрес пользователя
     * @return string
     */
    function getmyip()
    {
        $myip = file_get_contents('https://api.ipify.org');
        return $myip;
    }

    /**
     * Шифрует строку с примешиваем уникальной строки сайта $myhash
     * @param $text
     * @return string
     */
    function getmyhash($text){
        return $string = md5($text . $this->myhash);
    }
}

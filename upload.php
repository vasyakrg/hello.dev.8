<?php
require_once 'CUsers.php';
require_once 'CConfig.php';
$config = new Config();
$user = new CUsers();


if(isset($_FILES['uploadfile'])) {

    $uploaddir = './img/avatar/';
    $login = $_POST['login'];
    $uploadfile = $uploaddir . basename($login . '_' . time().'.png');
    if (copy($_FILES['uploadfile']['tmp_name'], $uploadfile)) {

        $user->updateavatar($login, substr($uploadfile, 2, strlen($uploadfile)));

        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".$config->address_site."profile.php");

    } else {
        echo "Ошибка! Не удалось загрузить файл на сервер!";
        exit;
    }
}

else
    exit("<p><strong>Ошибка!</strong> Вы зашли на эту страницу напрямую, поэтому нет данных для обработки. Вы можете перейти на <a href=".$config->address_site."> главную страницу </a>.</p>");

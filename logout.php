<?php
    //Запускаем сессию
    session_start();

    //Добавляем файл подключения к БД
    require_once("CToken.php");
    $token = new CToken();

//    if(isset($_COOKIE["token"]) && isset($_COOKIE["user"])) {
        $token->deletetokens($_COOKIE["tokenref"]);
//    }
//TODO предусмотреть механизм очистики базы токенов, если юзер почистил куки

    unset($_SESSION["user"]);
    unset($_SESSION["token"]);

    session_destroy();

    header("HTTP/1.1 301 Moved Permanently");
    header("Location: ".$config->address_site."index.php");
?>
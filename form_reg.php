<?php
//Запускаем сессию
session_start();

//Подключение шапки
require_once("header.php");

//Проверяем, если пользователь не авторизован, то выводим форму авторизации,
//иначе выводим сообщение о том, что он уже авторизован
if(!isset($_SESSION['user'])){
    ?>

    <div id="form_auth">
        <h2>Форма регистрации</h2>
        <form action="reg.php" method="post" name="form_reg" >
            <table>

                <tr>
                    <td> Новый логин: </td>
                    <td>
                        <input type="text" name="login" required="required" /><br />
                        <!--                        <span id="valid_login_message" class="mesage_error"></span>-->
                    </td>
                </tr>

                <tr>
                    <td> Пароль: </td>
                    <td>
                        <input type="text" name="password" placeholder="минимум 2 символов" required="required" /><br />
<!--                        <span id="valid_password_message" class="mesage_error"></span>-->
                    </td>
                <tr>
                    <td colspan="2" class="text_center" >
                        <label>
                            <input type="checkbox" name="remember_me" checked="checked" /> Запомнить меня
                        </label>
                    </td>
                </tr>
                <td>
                    <input type="submit" name="btn_submit_reg" value="Создать" />
                </td>
                <?php if (isset($_GET['c'])) echo 'такой логин уже занят!'; ?>
            </table>
        </form>
    </div>
    <?php
}else{
    ?>
    <div id="authorized">
        <h3>Вы уже авторизованы</h3>
        Перейти на <a href=".$config->address_site.">главную страницу</a>
    </div>

    <?php
}
?>

<?php

//Подключение подвала
require_once("footer.php");
?>
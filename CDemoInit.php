<?php
require_once 'CDB.php';

/**
 * Class CDemoInit
 * класс для начальной инициализации скриптов, базы, заполнения тестовыми данными
 */
class CDemoInit
{
  function initdb(){

  }

  function flushdemo(){

  }

    /**
     * Генерит посты и записывает в базу
     * @param $qposts   кол-во постов генерации (не рекомендуется более 500, большая нагрузка на базу!)
     * @param $qwords   кол-во случайных слов в поле post
     * @param $qusers   кол-во юзеров в базе
     * @return bool
     */
  function generatorposts($qposts, $qwords, $qusers){
      $words = $this->words2array('./demo/words.txt');

      $idusers = array();
      for ($i=0; $i<$qposts; $i++){
          $idusers[$i] = rand($qusers, 1);
      }

      $posts = array();
      for ($i=0; $i<$qposts; $i++){
          $posts[$i] = $this->getrandomwords($words, $qwords);
      }

      $db = new CDB();
      $result = $db->connect->prepare('insert into posts (iduser, post, postdate) values (:iduser, :post, :postdate)');
      $result->bindParam('iduser', $iduser);
      $result->bindParam(':post', $post);
      $result->bindParam(':postdate', $postdate);
      for ($i=0; $i<$qposts; $i++){
        $iduser = $idusers[$i];
        $post = $posts[$i];
        $postdate = date('Y-m-d H:i:s' , rand(strtotime('-1 year'), time()));
        $result->execute();
      }
      if ($result) return true;
      else return false;
  }

    /** Записывает в массив слова из словаря
     * @param $file путь до словаря на сервере
     * @return array|bool
     */
  function words2array($file){
      $words = file($file);
      for ($i=0; $i<count($words); $i++){
          $words[$i] = str_replace("\r\n", NULL, $words[$i]);
      }
      return $words;
  }

    /**
     * Генерит строку размером $qty из случайных слов на основе массива слов $arraywords, разделяя слова пробелом
     * @param $arraywords
     * @param $qty
     * @return string
     */
  function getrandomwords($arraywords, $qty){
      $str ='';
      for ($i=0; $i<=$qty; $i++) {
        $rand_keys = array_rand($arraywords);
        $str .= $arraywords[$rand_keys] . ' ';
      }
      return trim($str);
  }
}

//$demo = new CDemoInit();
//$demo->generatorposts(500,4,5);
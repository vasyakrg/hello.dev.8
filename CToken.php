<?php
require_once 'CDB.php';
require_once 'CUsers.php';
require_once 'CConfig.php';


/**
 * Class CToken
 * Класс для работы с токенами пользователей
 *
 */
class CToken
{
    /**
     * Проверяет наличие токена юзера в базе
     * @param $token
     * @return bool
     */
    function logintoken($token){
        $db = new CDB();
        $myresult = $db->connect->prepare('select count(token) from tokens where token = :token'); //и формирую запрос в базу
        $myresult->execute(array(':token' => $token)); // передаю запрос подставляя мои значениея в переменные с :

        if ($myresult->fetch(PDO::FETCH_NUM) > 0) return true; //вернет еденицу, если нашлась пара логин\пароль
        else return false; //token не нашелся
    }

    /**
     * Проверяет наличие рефреш-токена в базе по логину
     * @param $login
     * @return bool
     */
    function logintokenref($tokenref){
        $db = new CDB();
        $users = new CUsers();
        $myresult = $db->connect->prepare('select count(tokenref) from tokens where tokenref = :tokenref'); //и формирую запрос в базу
        $myresult->execute(array(':tokenref' => $tokenref)); // передаю запрос подставляя мои значениея в переменные с :

        if ($myresult->fetch(PDO::FETCH_NUM) > 0) return true; //вернет еденицу, если нашлась пара логин\пароль
        else return false; //tokenref не нашелся
    }

    /**
     * Формирует и записывает в таблицу новый токен юзера, выдает куки браузеру
     * @param $login
     *
     * @return bool false, если запись в базу не удалась
     */
    function createtokens($login){
        $db = new CDB();
        $users = new CUsers();
        $config = new Config();
        //TODO тут нужна сложная формула с привязкой к машине пользователя
        $token =    md5($users->getidfromlogin($login) . time() . $config->getmyhash($_SERVER['HTTP_USER_AGENT']));
        $tokenref = md5($users->getidfromlogin($login) . $config->getmyhash($_SERVER['HTTP_USER_AGENT']) . $config->getmyhash($config->myhash) . rand(1,1000));
        $myresult = $db->connect->prepare("insert into tokens (iduser, token, tokenref, lastdate, lastip) values (:userid, :token, :tokenref, current_timestamp, :lastip)");
        $myresult->execute(array(':userid' => $users->getidfromlogin($login), ':token' => $token, ':tokenref' => $tokenref, ':lastip' => $config->getmyip()));

        setcookie("token", $token, time() + (24 * 60 * 60));
        setcookie("tokenref", $tokenref, time() + (7 * 24 * 60 * 60));
        //Устанавливаем куку с логином
//        setcookie("user", $config->getmyhash($login), time() + (7 * 24 * 60 * 60));

        if ($myresult) return true; else false;
    }

    /**
     * Удалет токен юзера из таблицы, куки из браузера
     * @param $tokenref
     * @return bool false, если запись в базу не удалась
     */
    function deletetokens($tokenref){
        //стирает токен перед выходом
        $db = new CDB();
        $myresult = $db->connect->prepare("delete from tokens where tokenref = :tokenref");
        $myresult->execute(array(':tokenref' => $tokenref));

        setcookie("token", "", time() - 3600);
        setcookie("tokenref", "", time() - 3600);
//        setcookie("user", "", time() - 3600);

        if ($myresult) return true; else false;
    }

    /**
     * Генерирует и возвращает новый токен на основе рефреш-токена
     * @param $tokenref
     * @return string
     */
    function refreshtoken($tokenref){
        if (isset($tokenref))
            if ($this->logintokenref($tokenref)) {
                $db = new CDB();
                $config = new Config();

                $newtoken = md5(time() . $config->getmyhash($_SERVER['HTTP_USER_AGENT']));
                $myresult = $db->connect->prepare("update tokens set token = :newtoken, lastdate = current_timestamp, lastip = :lastip where tokenref = :tokenref");
                $myresult->execute(array(':newtoken' => $newtoken, ':lastip' => $config->getmyip(), 'tokenref' => $tokenref));

                setcookie("token", $newtoken, time() + (24 * 60 * 60));
            }
        return $newtoken;
    }

}
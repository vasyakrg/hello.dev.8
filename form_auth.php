<?php
//Запускаем сессию
session_start();

//Подключение шапки
require_once("header.php");
?>

    <script type="text/javascript">
        $(document).ready(function(){
            "use strict";
            //================ Проверка длины пароля ==================
            var password = $('input[name=password]');

            password.blur(function(){
                if(password.val() != ''){

                    //Если длина введенного пароля меньше шести символов, то выводим сообщение об ошибке
                    if(password.val().length < 2){
                        //Выводим сообщение об ошибке
                        $('#valid_password_message').text('Минимальная длина пароля 2 символа');

                        // Дезактивируем кнопку отправки
                        $('input[type=submit]').attr('disabled', true);

                    }else{
                        // Убираем сообщение об ошибке
                        $('#valid_password_message').text('');

                        //Активируем кнопку отправки
                        $('input[type=submit]').attr('disabled', false);
                    }
                }else{
                    $('#valid_password_message').text('Введите пароль');
                }
            });
        });
    </script>

<?php
//Проверяем, если пользователь не авторизован, то выводим форму авторизации,
//иначе выводим сообщение о том, что он уже авторизован
if(!isset($_SESSION['user'])){
    ?>

    <div id="form_auth">
        <h2>Форма авторизации</h2>
        <form action="auth.php" method="post" name="form_auth" >
            <table>

                <tr>
                    <td> Login: </td>
                    <td>
                        <input type="text" name="login" required="required" value="vasya" /><br />
<!--                        <span id="valid_login_message" class="mesage_error"></span>-->
                    </td>
                </tr>

                <tr>
                    <td> Password: </td>
                    <td>
                        <input type="text" name="password" placeholder="минимум 2 символов" required="required" value="123" /><br />
                        <span id="valid_password_message" class="mesage_error"></span>
                    </td>
                </tr>

<!--                <tr>-->
<!--                    <td> Введите капчу: </td>-->
<!--                    <td>-->
<!--                        <p>-->
<!--                            <img src="captcha.php" alt="Капча" /> <br />-->
<!--                            <input type="text" name="captcha" placeholder="Проверочный код" />-->
<!--                        </p>-->
<!--                    </td>-->
<!--                </tr>-->
<!--                <tr>-->
                    <td colspan="2" class="text_center" >
                        <label>
                            <input type="checkbox" name="remember_me" checked="checked" /> Запомнить меня
                        </label>
                    </td>
                </tr>

                <tr>
                    <td>
                        <input type="submit" name="btn_submit_auth" value="Войти" />
                    </td>
                    <?php if (isset($_GET['b'])) echo 'не верная пара логин\пасс!'; ?>
<!--                    <td>-->
<!--                        <a href="reset_password.php">Забыли пароль?</a>-->
<!--                    </td>-->
                </tr>
            </table>
        </form>
    </div>
    <?php
}else{
    ?>
    <div id="authorized">
        <h3>Вы уже авторизованы</h3>
       Перейти на <a href=".$config->address_site.">главную страницу</a>
    </div>

    <?php
}
?>

<?php

//Подключение подвала
require_once("footer.php");
?>
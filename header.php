<?php
session_start();

//Добавляем файл подключения к БД
require_once 'CDB.php';
require_once 'CToken.php';
require_once 'CUsers.php';

$db = new CDB();
$token = new CToken();
$user = new CUsers();

if(isset($_COOKIE["token"]) && (isset($_SESSION["user"]))) {
    //TODO при учнижножении сесии на сервере авторизация падает даже при куках!
    if ($token->logintoken($_COOKIE["token"], $_SESSION["user"])) //token есть в базе
    {
        $_SESSION['token'] = $_COOKIE["token"]; //ставим токен с куков в сессию
//        $_SESSION['user'] = $user->getloginfromtoken($_COOKIE["token"]);
    }

}

//проверяет токен и если его нет, пытаемся рефрешить новые на основе tokenref
if (!isset($_COOKIE['token'])) {
    $_SESSION['token'] = $token->refreshtoken($_COOKIE['tokenref']);
//    $_SESSION['user'] = $user->getloginfromtoken($_COOKIE["token"]);
}

//if (!isset($_COOKIE['user']))
//{
//    unset($_SESSION["user"]);
//    unset($_SESSION["token"]);
//}

require_once 'header_.php';
